package de.jottyfan.worktimelog.view.panel;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * 
 * @author jotty
 *
 */
public class BodyPanel extends Panel {
	private static final long serialVersionUID = 1L;

	public BodyPanel(String id, IModel<?> model) {
		super(id, model);
	}

	public BodyPanel(String id) {
		super(id);
	}
}
