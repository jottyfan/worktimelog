package de.jottyfan.worktimelog.view.form;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.Model;

import de.jottyfan.worktimelog.view.Login;
import de.jottyfan.worktimelog.view.Welcome;

/**
 * 
 * @author jotty
 *
 */
public class LoginForm extends Form<Void> {
	private static final long serialVersionUID = 1L;

	private final TextField<String> usernameField;
	private final PasswordTextField passwordField;

	public LoginForm(String id) {
		super(id);
		usernameField = new TextField<String>("username", Model.of(""));
		passwordField = new PasswordTextField("password", Model.of(""));

		add(usernameField);
		add(passwordField);
	}

	public final void onSubmit() {
		String username = (String) usernameField.getDefaultModelObject();
		String password = (String) passwordField.getDefaultModelObject();
		// TODO: check password credentials
		if (username.equals("jotty")) {
			setResponsePage(Welcome.class);
		} else {
			setResponsePage(Login.class);
		}
	}
}
