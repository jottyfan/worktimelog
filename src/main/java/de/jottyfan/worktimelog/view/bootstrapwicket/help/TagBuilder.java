package de.jottyfan.worktimelog.view.bootstrapwicket.help;

/**
 * 
 * @author jotty
 *
 */
public class TagBuilder {
	private final StringBuilder buf;

	public TagBuilder() {
		buf = new StringBuilder();
	}

	public TagBuilder appendIf(boolean condition, Object... o) {
		if (condition) {
			for (Object current : o) {
				buf.append(current);
			}
		}
		return this;
	}

	public TagBuilder append(Object... o) {
		return appendIf(true, o);
	}

	public String toString() {
		return buf.toString();
	}
}
