package de.jottyfan.worktimelog.view.panel;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * 
 * @author jotty
 *
 */
public class MenuPanel extends Panel {
	private static final long serialVersionUID = 1L;

	public MenuPanel(String id, IModel<?> model) {
		super(id, model);
	}

	public MenuPanel(String id) {
		super(id);
	}
}
