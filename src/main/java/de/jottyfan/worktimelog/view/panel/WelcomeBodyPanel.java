package de.jottyfan.worktimelog.view.panel;

import org.apache.wicket.model.IModel;

import de.jottyfan.worktimelog.view.bootstrapwicket.Card;

/**
 * 
 * @author jotty
 *
 */
public class WelcomeBodyPanel extends BodyPanel {
	private static final long serialVersionUID = 1L;

	public WelcomeBodyPanel(IModel<?> model) {
		super("bodyPanel", model);
		setup();
	}

	public WelcomeBodyPanel() {
		super("bodyPanel");
		setup();
	}

	private void setup() {
		Card card = new Card("welcomeCard");
		card.setTitle("Herzlich Willkommen");
		card.setBody("... in der Welt von Wicket.");
		card.setRenderFooter(false);
		card.getCss().put("margin", "24px");
		card.getCss().put("box-shadow", "3px 2px 4px 0px rgba(119,119,119,0.5)");
		card.getTitleCss().put("background-image", "linear-gradient(to bottom right, #007bff, #4d81b9)");
		card.getTitleCss().put("color", "white");
		card.getTitleCss().put("font-weight", "bolder");
		card.getTitleCss().put("text-shadow", "1px 1px darkblue");
		add(card);
	}
}
