package de.jottyfan.worktimelog.view.bootstrapwicket;

import java.util.HashMap;
import java.util.Map;

import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.MarkupStream;
import org.apache.wicket.markup.html.WebComponent;
import org.apache.wicket.model.IModel;

import de.jottyfan.worktimelog.view.bootstrapwicket.help.Styler;
import de.jottyfan.worktimelog.view.bootstrapwicket.help.TagBuilder;

/**
 * 
 * @author jotty
 *
 */
public class Card extends WebComponent {
	private static final long serialVersionUID = 1L;
	private String title;
	private String body;
	private String footer;
	private final Map<String, String> css;
	private final Map<String, String> titleCss;
	private final Map<String, String> bodyCss;
	private final Map<String, String> footerCss;

	private boolean renderTitle;
	private boolean renderBody;
	private boolean renderFooter;

	public Card(String id, IModel<?> model) {
		super(id, model);
		renderTitle = true;
		renderBody = true;
		renderFooter = true;
		this.css = new HashMap<>();
		this.titleCss = new HashMap<>();
		this.bodyCss = new HashMap<>();
		this.footerCss = new HashMap<>();
	}

	public Card(String id) {
		super(id);
		renderTitle = true;
		renderBody = true;
		renderFooter = true;
		this.css = new HashMap<>();
		this.titleCss = new HashMap<>();
		this.bodyCss = new HashMap<>();
		this.footerCss = new HashMap<>();
	}

	@Override
	public void onComponentTagBody(MarkupStream markupStream, ComponentTag openTag) {
		TagBuilder buf = new TagBuilder();
		buf.append("<div class=\"card\" wicket:id=\"", getId(), "\" ", Styler.writeIf(css.size() > 0, css), ">");
		buf.appendIf(renderTitle, "  <div class=\"card-header\"", Styler.writeIf(titleCss.size() > 0, titleCss), ">", title,
				"</div>");
		buf.appendIf(renderBody, "  <div class=\"card-body\"", Styler.writeIf(bodyCss.size() > 0, bodyCss), ">", body,
				"</div>");
		buf.appendIf(renderFooter, "  <div class=\"card-footer\"", Styler.writeIf(footerCss.size() > 0, footerCss), ">",
				footer, "</div>");
		buf.append("</div>");
		replaceComponentTagBody(markupStream, openTag, buf.toString());
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *          the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * @param body
	 *          the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}

	/**
	 * @return the footer
	 */
	public String getFooter() {
		return footer;
	}

	/**
	 * @param footer
	 *          the footer to set
	 */
	public void setFooter(String footer) {
		this.footer = footer;
	}

	/**
	 * @return the renderTitle
	 */
	public boolean isRenderTitle() {
		return renderTitle;
	}

	/**
	 * @param renderTitle
	 *          the renderTitle to set
	 */
	public void setRenderTitle(boolean renderTitle) {
		this.renderTitle = renderTitle;
	}

	/**
	 * @return the renderBody
	 */
	public boolean isRenderBody() {
		return renderBody;
	}

	/**
	 * @param renderBody
	 *          the renderBody to set
	 */
	public void setRenderBody(boolean renderBody) {
		this.renderBody = renderBody;
	}

	/**
	 * @return the renderFooter
	 */
	public boolean isRenderFooter() {
		return renderFooter;
	}

	/**
	 * @param renderFooter
	 *          the renderFooter to set
	 */
	public void setRenderFooter(boolean renderFooter) {
		this.renderFooter = renderFooter;
	}

	/**
	 * @return the css
	 */
	public Map<String, String> getCss() {
		return css;
	}

	/**
	 * @return the titleCss
	 */
	public Map<String, String> getTitleCss() {
		return titleCss;
	}

	/**
	 * @return the bodyCss
	 */
	public Map<String, String> getBodyCss() {
		return bodyCss;
	}

	/**
	 * @return the footerCss
	 */
	public Map<String, String> getFooterCss() {
		return footerCss;
	}

}
