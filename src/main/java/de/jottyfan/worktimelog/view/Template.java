package de.jottyfan.worktimelog.view;

import org.apache.wicket.markup.html.WebPage;

import de.jottyfan.worktimelog.view.panel.BodyPanel;
import de.jottyfan.worktimelog.view.panel.MenuPanel;

/**
 * 
 * @author jotty
 *
 */
public abstract class Template extends WebPage {
	private static final long serialVersionUID = 1L;

	public Template() {
		setupTemplate();
	}

	private void setupTemplate() {
		MenuPanel menuPanel = defineMenu();
		BodyPanel bodyPanel = defineBody();
		add(menuPanel == null ? new MenuPanel("menuPanel") : menuPanel);
		add(bodyPanel == null ? new BodyPanel("bodyPanel") : bodyPanel);
	}
	
	protected abstract MenuPanel defineMenu();
	protected abstract BodyPanel defineBody();
}
