package de.jottyfan.worktimelog.view;

import de.jottyfan.worktimelog.view.panel.BodyPanel;
import de.jottyfan.worktimelog.view.panel.LogoutMenuPanel;
import de.jottyfan.worktimelog.view.panel.MenuPanel;
import de.jottyfan.worktimelog.view.panel.WelcomeBodyPanel;

/**
 * 
 * @author jotty
 *
 */
public class Welcome extends Template {
	private static final long serialVersionUID = 1L;

	public Welcome() {
	}

	@Override
	protected MenuPanel defineMenu() {
		return new LogoutMenuPanel();
	}

	@Override
	protected BodyPanel defineBody() {
		return new WelcomeBodyPanel();
	}
}
