package de.jottyfan.worktimelog;

import org.apache.wicket.Page;
import org.apache.wicket.protocol.http.WebApplication;

import de.jottyfan.worktimelog.view.Login;

/**
 * 
 * @author jotty
 *
 */
public class WorktimelogApplication extends WebApplication {

	public WorktimelogApplication() {
		// disables ajax per default
	}
	
	@Override
	public Class<? extends Page> getHomePage() {
		return Login.class;
	}
}
