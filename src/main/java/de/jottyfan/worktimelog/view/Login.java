package de.jottyfan.worktimelog.view;

import de.jottyfan.worktimelog.view.panel.BodyPanel;
import de.jottyfan.worktimelog.view.panel.EmptyMenuPanel;
import de.jottyfan.worktimelog.view.panel.LoginPanel;
import de.jottyfan.worktimelog.view.panel.MenuPanel;

/**
 * 
 * @author jotty
 *
 */
public class Login extends Template {
	private static final long serialVersionUID = 1L;

	@Override
	protected BodyPanel defineBody() {
		return new LoginPanel();
	}

	@Override
	protected MenuPanel defineMenu() {
		return new EmptyMenuPanel();
	}
}
