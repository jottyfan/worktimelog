package de.jottyfan.worktimelog.view.panel;

import org.apache.wicket.model.IModel;

/**
 * 
 * @author jotty
 *
 */
public class EmptyMenuPanel extends MenuPanel {
	private static final long serialVersionUID = 1L;

	public EmptyMenuPanel(IModel<?> model) {
		super("menuPanel", model);
	}

	public EmptyMenuPanel() {
		super("menuPanel");
	}
}
