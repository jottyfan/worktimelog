package de.jottyfan.worktimelog.view.bootstrapwicket.help;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * 
 * @author jotty
 *
 */
public class Styler {

	public static final String writeIf(boolean condition, Map<String, String> map) {
		if (condition) {
			StringBuilder buf = new StringBuilder("style=\"");
			Iterator<Entry<String, String>> i = map.entrySet().iterator();
			while (i.hasNext()) {
				Entry<String, String> entry = i.next();
				buf.append(entry.getKey()).append(": ").append(entry.getValue()).append(i.hasNext() ? "; " : "");
			}
			buf.append("\"");
			return buf.toString();
		} else {
			return "";
		}
	}
}
