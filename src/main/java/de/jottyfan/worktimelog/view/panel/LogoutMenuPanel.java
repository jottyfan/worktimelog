package de.jottyfan.worktimelog.view.panel;

import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;

import de.jottyfan.worktimelog.view.Login;

/**
 * 
 * @author jotty
 *
 */
public class LogoutMenuPanel extends MenuPanel {
	private static final long serialVersionUID = 1L;

	public LogoutMenuPanel(IModel<?> model) {
		super("menuPanel", model);
		setup();
	}

	public LogoutMenuPanel() {
		super("menuPanel");
		setup();
	}

	public void setup() {
		add(new Link<Void>("logoutMenuButton") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				setResponsePage(Login.class);
			}
		});
	}
}
