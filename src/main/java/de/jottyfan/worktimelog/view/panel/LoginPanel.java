package de.jottyfan.worktimelog.view.panel;

import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;

import de.jottyfan.worktimelog.view.Welcome;
import de.jottyfan.worktimelog.view.form.LoginForm;

/**
 * 
 * @author jotty
 *
 */
public class LoginPanel extends BodyPanel {
	private static final long serialVersionUID = 1L;

	public LoginPanel(IModel<?> model) {
		super("bodyPanel", model);
		setup();
	}

	public LoginPanel() {
		super("bodyPanel");
		setup();
	}

	private void setup() {
		add(new LoginForm("loginForm"));
		
		add(new Link<Void>("loginLink") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				setResponsePage(Welcome.class);
			}
		});
	}
}
